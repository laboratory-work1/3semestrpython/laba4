import random
import matplotlib.pyplot as plt
from math import pi

def pi_mk(n):
    """Функция считающая число Пи методом Монте-карло.

    На вход подается колличество испытаний.
    На выходе получается приближенное число Пи.
    """
    k = 0.0
    for i in range(1, n):
        x = random.random()
        y = random.random()
        k += (x * x + y * y < 1.0)

    return (4 * k / n)

val_pi_mk = dict()

for i in range(1000, 151000 + 1, 5000):

    val_pi_mk[i] = pi_mk(i)

plt.plot(val_pi_mk.keys(), val_pi_mk.values(), linestyle = '--', marker = 'o', color = 'k', markersize = '4')
plt.axhline(pi, color = 'r')
plt.ylim(2.9, 3.3)
plt.show()
